<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
  <div id='wrapper'>
    <?php include ('headerPage.php');?>
  </div>
  <!--work container-->
  <div class='workHeaderContainer'>
    <div class='container'>
      <div class='workHeader'>
        BookMyCab
        <h1>Android App</h1>
      </div>
    </div>
  </div>
  <div class='aboutWorkBox'>
     <div class='container'>
      <div class='projectThumbnail col-md-8 col-sm-12'>
      <img src='images/BookmyCab-Phone-Screen_lg.jpg' alt='BookmyCab' />
      </div>
      <div class='workDescription col-md-4 col-sm-12'>
        <div class='workDiscriptionTitle'>
          <strong>CLIENT</strong>
          <br />
          BookMyCab
        </div>
        <p>BookMyCab is the first and only Cab Service that has tied up with the Black and Yellow metered Taxis. BookMyCab already had a Android based application ready when they approached the
        Geeks. The only problem was that the application was lacking the User Interface. Get the design wrong and next thing you know is that the ever moving Mumbaikars have switched to a competitor
        app service. Huge responsibility rested on the shoulders of our Design Geeks but you just need to sit, relax and have a beer when you 'Leave IT to the Geeks'.
        </p>
        <p>After rounds of iterations we came up with the final design that clicked with the users. BookMyCab got a great feedback from their customers about the app and the design. </p><br />
        <p>You can download it here</p>
        <a href='https://play.google.com/store/apps/details?id=com.mobile.bookmycab.com' target='_blank'><img src='images/google_play_icon.jpg' alt='Google Play' /></a>
      </div>
    </div>
  </div>
  <a class='proNav proPrev' href='reliance.php' title='Previous'></a>
  <a class='proNav proNext' href='Golfingindian.php' title='Next'></a>
  <!-- Last Text Note -->
  <?php include ('footer.php');?>
  <!-- Last Text Note end-->
  <?php //include ('assetPageJs.php');?>
  <?php include ('assetJs.php');?>
  </body>
</html>