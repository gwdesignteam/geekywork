<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
  <div id='wrapper'>
    <?php include ('headerPage.php');?>
  </div>
  <!--work container-->
  <div class='workHeaderContainer'>
    <div class='container'>
      <div class='workHeader'> Golfingindian.com
        <h1>Web Design & Development</h1>
      </div>
    </div>
  </div>
  <div class='aboutWorkBox'>
    <div class='container'>
      <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
        <img src='images/Golfingindian-Mac-Screen_lg.jpg' class='Golfingindian'/> 
      </div>
      <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
        <div class='workDiscriptionTitle'>
          <strong>CLIENT</strong> <br />
          Golfing Indian 
        </div>
        <br />
        <p>Golfingindian.com  website is owned and operated by Ms. Shaili Chopra, receipient of Ramnath Goenka Award for Excellence in Business Journalism. Ms. Shaili is currently the Business Editor 
        for Tehelka. Golfing Indian's design had become outdated and was no longer able to impress its target audience. Navigating through the website was incoherent and hard to follow. 
        The homepage was offering way too much information in an unorganized manner.Golfing Indian decided to 'Leave IT to the Geeks'. We completely redesigned the website and made sure that it is 
        an exclusive blend of creative conception and interactive architecture. We got rid of the clutter from the website and made it look neat and sophisticated. The new website visually ties in with the </p>
        <br />
        <p>
        <p>You can visit the website at:</p>
        <p class='projectUrl'><a href='http://golfingindian.com/' target='_blank' style='text-decoration:none;'>www.golfingindian.com</a> </p>
      </div>
    </div>
  </div>
<a class='proNav proPrev' href='BookMyCab.php' title='Previous'></a>
<a class='proNav proNext' href='upme.php' title='Next'></a>
<!-- Last Text Note -->
<?php include ('footer.php');?>
<!-- /Last Text Note -->
<?php //include ('assetPageJs.php');?>
<?php include ('assetJs.php');?>
</body>
</html>