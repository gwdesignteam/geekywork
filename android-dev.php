<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
		<meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
		<title>.:: ANDROID APP DEVELOPMENT ::.</title>
		<?php include ('assetCss.php');?>
	</head>
	<body>
		<div id='wrapper'>
			<?php include ('headerPage.php');?>
		</div>
		<!-- work container -->
		<div class='workHeaderContainer'>
			<div class='container'>
				<h1 class='workHeader'>
				ANDROID APP
				<br />
				DEVELOPMENT</h1>
			</div>
		</div>
		<div class='aboutServiceBox'>
			<div class='container'>
				<div class='serviceBox col-md-8 col-sm-12 col-xs-12'>
					<p>As the internet, phone and camera functionalities move from PCs, laptops to smart mobile devices, the need to keep your business updated is more than ever. Recent years have seen a massive shift in the usage pattern of mobile phone as mobile applications play a vital role in it. Be it a memory app, travel app or any other category. If you can think it, an app can be made for it.</p><br />
					<p>We exist to make it easy for you to get your mobile application developed and deployed. We have a collection of world class user interface designers and coders with relevant experience. Our team drives the analysis, analytics and integration of mobile application development projects and can create a world class apps for Android.</p>
					<br />
					<h3>We have :</h3>
					<ul class='weHaveList'>
					<li>Excellent user interface designers</li>
					<li>Cost effective development process</li>
					<li>Experienced android app developers</li>
					<li>Smooth functionality across different android devices</li>
					<li>Expertise to develop Android applications across a large number of categories</li>
					<li>Willingness to sign NDA with you to keep your unique ideas safe and under your copyright</li>
					</ul>
					<br />
					<h3>Why us?</h3>
					<br />
					<h4>Endless Creativity :</h4>
					<p>Geeky Works is actively involved in developing Android applications that are technically accurate and visibly beautiful. From taking a mobile application development project from a scratch or adding features to an already existing Android application, we have the right design and technical experience that guarantees successful completion of your project.</p><br />
					<h4>Multi-Platform Exposure :</h4>
					<p>Android application development task requires exposure to different languages and user interface experiences. It gives us the freedom to build custom apps that are native to each platform.</p>
					<br />
					<p>If you are looking to enhance your existing Android applications or want a completely new one to be developed and deployed within a strict deadline, let us know by clicking here.</p>
					<br /><br />
					<a href='index.php#contact'><img src='images/conatct_button.jpg' width='200' height='80' alt='contact' /></a>
				</div>
				<div class='serviceImg col-md-4 col-sm-12 col-xs-12'>
					<img src='images/Android_dev_ig.png' alt='Android Development' />
				</div>
				<div class='clearfix'></div>
			</div>
		</div>
	<?php include ('footer.php');?>
	<!-- /Last Text Note -->
	<?php include ('assetJs.php');?>
	</body>
</html>