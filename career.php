<?php
require("class.phpmailer.php");
if(isset($_POST['submit'])) {
$name = trim($_POST['name']);
$email = trim($_POST['email']);
$contact = trim($_POST['telephone']);
$position_applied = trim($_POST['position_applied_for']);
$filename = $_FILES["resume"]['tmp_name'];
if($position_applied == '') {
$Subject1 = "Application for job";
} else {
$Subject1 = "Application for post of ". $position_applied;
}
if(!filter_var($email, FILTER_VALIDATE_EMAIL))
{
$msg = "Please enter valid email id";
} else if($filename == '') {
$msg = "Please attached your resume";
} else {
$mail = new PHPMailer();
$mail->isHTML(true);
$mail->AddAttachment($filename,$_FILES['resume']['name']);
//$mail->From = $email;
$mail->SetFrom($email, $name);
$address = "contact@geekyworks.com";
$mail->AddAddress($address);
//$mail->AddReplyTo($email,$email);
$mail->Subject = $Subject1;
$message='';
$message = '<html><body>';
$message .= '<table cellpadding="10" width="100%">';
$message .= "<tr><td >Hello Sir,</td></tr>";
$message .= "<tr><td ><b>".$name."</b> has send you resume. Details are as follows.</td></tr>";
$message .= "<tr><td ></td><td ></td></tr>";
$message .= "<tr><td><strong>Name:</strong>&nbsp;&nbsp;".$name." </td></tr>";
$message .= "<tr><td><strong>Email:</strong>&nbsp;&nbsp;".$email." </td></tr>";
$message .= "<tr><td><strong>Phone Number:</strong>&nbsp;&nbsp;".$contact." </td></tr>";
$message .= "<tr><td ></td><td ></td></tr>";
$message .= "<tr><td >Thanks,</td></tr>";
$message .= "<tr><td >".$name."</td></tr>";
$message .= "</table>";
$message .= "</body></html>";
$mail->Body = $message;
$mail->WordWrap = 50;
if(!$mail->Send()) {
$msg = "Error occured";
//echo "Mailer Error: " . $mail->ErrorInfo;
} else {
$msg = "Thanks";
//echo "Message sent!".$mail->ErrorInfo;
}
}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta charset='utf-8'>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
<meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
<title>.:: Career with us ::.</title>
<?php include ('assetCss.php');?>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="jquery.bpopup-x.x.x.min.js"></script>
<script>
$('element_to_pop_up').bPopup({
content:'iframe', //'ajax', 'iframe' or 'image'
contentContainer:'.content',
loadUrl:'http://dinbror.dk/search' //Uses jQuery.load()
});
</script>-->
<!--script for collapse-->
<script type="text/javascript">
var collapseDivs, collapseLinks;
function createDocumentStructure (tagName) {
if (document.getElementsByTagName) {
var elements = document.getElementsByTagName(tagName);
collapseDivs = new Array(elements.length);
collapseLinks = new Array(elements.length);
for (var i = 0; i < elements.length; i++) {
var element = elements[i];
var siblingContainer;
if (document.createElement &&
(siblingContainer = document.createElement('div')) &&
siblingContainer.style)
{
var nextSibling = element.nextSibling;
element.parentNode.insertBefore(siblingContainer, nextSibling);
var nextElement = elements[i + 1];
while (nextSibling != nextElement && nextSibling != null) {
var toMove = nextSibling;
nextSibling = nextSibling.nextSibling;
siblingContainer.appendChild(toMove);
}
siblingContainer.style.display = 'none';
collapseDivs[i] = siblingContainer;
createCollapseLink(element, siblingContainer, i);
}
else {
// no dynamic creation of elements possible
return;
}
}
createCollapseExpandAll(elements[0]);
}
}
function createCollapseLink (element, siblingContainer, index) {
var span;
if (document.createElement && (span = document.createElement('span'))) {
span.appendChild(document.createTextNode(String.fromCharCode(160)));
var link = document.createElement('a');
link.collapseDiv = siblingContainer;
link.href = '#';
link.appendChild(document.createTextNode('+'));
link.onclick = collapseExpandLink;
collapseLinks[index] = link;
span.appendChild(link);
element.appendChild(span);
}
}
function collapseExpandLink (evt) {
if (this.collapseDiv.style.display == '') {
this.parentNode.parentNode.nextSibling.style.display = 'none';
this.firstChild.nodeValue = '+';
}
else {
this.parentNode.parentNode.nextSibling.style.display = '';
this.firstChild.nodeValue = '-';
}
if (evt && evt.preventDefault) {
evt.preventDefault();
}
return false;
}
function expandAll (evt) {
for (var i = 0; i < collapseDivs.length; i++) {
collapseDivs[i].style.display = '';
collapseLinks[i].firstChild.nodeValue = '-';
}
if (evt && evt.preventDefault) {
evt.preventDefault();
}
return false;
}
function collapseAll (evt) {
for (var i = 0; i < collapseDivs.length; i++) {
collapseDivs[i].style.display = 'none';
collapseLinks[i].firstChild.nodeValue = '+';
}
if (evt && evt.preventDefault) {
evt.preventDefault();
}
return false;
}
function validateFrm()
{
var uname = $("#uname").val();
var contact = $("#contact").val();
var email = $("#email").val();
//var message = $("#message").val();
var atpos = email.indexOf("@");
var dotpos = email.lastIndexOf(".");
if (uname == "") {
$("#uname").focus();
alert('Please enter your user name');
return false;
}
if (email == "") {
$("#email").focus();
alert('Please enter your email id');
return false;
}
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
alert("Not a valid e-mail address");
$("#email").focus();
return false;
}
if(contact == "") {
$("#contact").focus();
alert('Please enter your contact number');
return false;
}
if(isNaN(contact))
{
alert("Enter the valid contact Number");
$("#contact").focus();
return false;
}
if(contact.length < 10 ) {
$("#contact").focus();
alert('Please enter Valid contact number');
return false;
}
}
</script>
<script type="text/javascript">
window.onload = function (evt) {
createDocumentStructure('h5');
}
</script>
<!--end script for collapse-->
</head>
<body>
<!-- Header -->
<div id='wrapper'>
<?php include ('headerPage.php');?>
</div>
<!--work container-->
<div class="media"><!--media start-->
<div class="title">
<p>Careers</p>
</div>
<div class="careerContainer container">
<div class="careerList col-sm-4">
<div><img src="images/Kid_Image.png"/></div>
</div>
<div class="careerListContent col-sm-8">
<div class="award-title">Make your Momma Proud</div>
<div class="award-details">
<p>Does the phrase 'outside the box' make you cringe because it's too inside the box? Had you always wanted to work for an organization where you can give vent to your creativity? </p>
<p>If the answer to these questions is 'yes' then you have landed at the right spot. At GeekyWorks we are always on the lookout for awesome coders who code not just to get the job done but because they genuinely love it. </p>
<p>If you are one of those code-loving geeks who is ready to make the world a better place while having loads of fun then Geeky Works is the place for you. </p>
</div>
</div>
</div>
</div>
<!--media end-->
<!-- Career Oportunities-->
<div class="careerForm">
<div class="careerOportunity container">
<div class="careerOpt col-sm-4">
<div class="title">
<p>Career Opportunities </p>
</div>
<div class="career">
<div class="webDesign">
<h5>Web Design</h5>
<!--Role : <input type="text" /> <br />
Exp. : <input type="text" />-->
<!--<label> Role : </label>-->
<p> Geeky Works is a Website Design Company based in Pune and we are always on the lookout for exceptional designers.<br/>
For us, the right person for the job is someone who has an eye for accuracy and is extremely detail oriented.
<!--<span style="color:#ddd;"> Read More...</span>-->
</p>
<hr />
<label> To Apply you must : </label>
<ul>
<li> Have atleast 2 years of experience designing websites</li>
<li> Have experience with Responsive Design</li>
<li> Have solid knowledge of Photoshop and Illustrator</li>
<li> Have good understanding of HTML/CSS and Javascript interaction methods </li>
</ul>
</div>
</div>
</div>
<!-- end Career Oportunities-->
<!--- Career Form -->
<div class="formCareer col-sm-8">
<div class='row'>
<form method="POST" action="" onsubmit="return validateFrm();" enctype="multipart/form-data" class='col-sm-12'>
<div class='row'>
<div class="col-sm-12">
<input type="text" name="name" id="uname" placeholder="name_" class="careerTextfield" required="required">
</div>
<div class="col-sm-6">
<input type="text" name="email" id="email" placeholder="email id_" class="careerTextfield" required="required">
</div>
<div class="col-sm-6">
<input type="text" name="telephone" id="contact" placeholder="phone number_" class="careerTextfield" required="required">
</div>
<div class="col-sm-12">
<input type="text" name="position_applied_for" placeholder ="position_applied_for_" class="careerTextfield" required="required">
</div>
<div class="col-sm-12">
<input type="file" class="uploadcv" name="resume" required="required" />
</div>
<div class="col-sm-12">
<input class="careerButton col-sm-12" type="submit" name="submit" value="">
</div>
</div>
</form>
</div>
<div class='clearfix'></div>
</div>
</div>
</div>
<!--- /Contact Form -->
<!-- Last Text Note -->
<?php include ('footer.php');?>
<!-- Last Text Note -->
<?php //include ('assetPageJs.php');?>
<?php include ('assetJs.php');?>
</body>
</html>
