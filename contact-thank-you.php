<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <meta property='og:image' content='http://geekyworks.com/images/gw_logo.jpg' />
  <meta name='title' content='Award Winning Web and Mobile Application development company'/>
  <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
  <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
  <title>Award Winning Web and Mobile Application development company</title>
  <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon'>
  <link rel='icon' href='/favicon.ico' type='image/x-icon'>
  <link type='text/css' href='css/demo.css' rel='stylesheet' media='screen' />
  <?php include ('assetCss.php');?>
  <?php include ('assetJs.php');?>
  <script src='jquery.bpopup-x.x.x.min.js'></script>
  <script>
    $('element_to_pop_up').bPopup({
      content:'iframe', //'ajax', 'iframe' or 'image'
      contentContainer:'.content',
      loadUrl:'http://dinbror.dk/search' //Uses jQuery.load()
    });
  </script>
</head>
<body>
  <?php include ('headerPage.php');?>
  <div>
  <!--work container-->
  <div class='container'>
    <div class='ThankyouMsgWrapp'>
    <h1>Thank You</h1>
    <h3>your message has been sent!</h3>
    </div>
    </div>
  </div>
</body>
</html>

