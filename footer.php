<div class='footerWrapp'>
	<div class='container'>
		<div class='footerMsg'> it was nice of you to drop by
			<input type='text' id='textFour' value=''/>
			<br />
			have a pixel perfect day !
		</div>
		<div class='bottomFooter'>
			<div class='contactDetails'>
				<h2 class='contactHeading'>contact</h2>
				<div class='col-sm-5'>
					<p class='contactIndiaTitle'>India</p>
					<p class='address'>Office No. 6,<br />
					Aditi Commerce Building,</br>
					Opposite Bank of Maharashtra<br />
					Baner, Pune - 411045</p>
					<p class='phoneNumber'>08600739763 / 08826407004 </p>
					<p class='emailId'>contact@geekyworks.com </p>
				</div>
				<div class='col-sm-5'>
					<p class='contactUkTitle'>UK</p>
					<p class='address'>39 elk path<br />
							Three mile cross<br />
							Reading
							RG7 1WE</p>
					<br />
					<p class='phoneNumber'>07435363390</p>
					<p class='emailId'>contact@geekyworks.com </p>
				</div>
				<div class='col-sm-2 socialIcons'>
					<h2 class='contactHeading'>connect</h2>
					<a href='https://www.facebook.com/GeekyWorks/' target='_blank'>
					<p class='fbLink'>facebook </p>
					</a>
					<p class='skypeLink'>geekyworks</p>
					<a href='http://www.linkedin.com/company/geeky-works?trk=company_name' target='_blank'>
					<p class='lnLink'>linkedin </p>
					</a>
				</div>
			</div>
		</div>

	</div>
	<div class='clearfix'></div>
</div>
<!--teamMember01 modal -->
<div class='modal fade teamMemberPop' id='teamMember01' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/anant.png' alt='Anant'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							`If it isn’t perfect, it isn’t right` that’s the motto of our CEO. Perfection is his second nature and although this comes as a boon to all the clients, it is a cause of royal pain to the poor mortals at Geeky Works. Anant received his bachelors in Marketing and International Business from Temple University, Philadelphia, where the seeds of entrepreneurship germinated in his mind. He took up various service opportunities, worked as an SEO consultant with Seer Interactive, one of the fastest growing firms in Pennsylvania and led the marketing division of Alliance Max. But his insatiable desire to innovate couldn`t hold him back from starting on his own. As one of the founding members of Geeky Works, Anant takes care of pretty much everything (except coding). And, when he is not experimenting with one of his outrageous marketing ideas (some of them accidentally turn out to be brilliant), Anant is often found playing(being) the juke box in office (sings on request for his geeks).
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember01 modal -->
<!--teamMember02 modal -->
<div class='modal fade teamMemberPop' id='teamMember02' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/satyendra.png' alt='satyendra'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							Satya holds a Bachelor`s Degree in Electronics from IIT BHU, India and is driving most crucial part i.e. development and technology for Geekywork`s Indian operations. Satyendra is responsible for project delivery and managing the technology group within Geekyworks. He plays an important role in setting technical standards and has been active in establishing company-wide processes and practices. He has extensive experience in client-server applications, Cloud integration solutions, mobile application development and is often responsible for identifying key new technologies to invest in. In his free time, Satyendra enjoys maintaining his Hindi poetry blog and trying out various international cuisines.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember02 modal -->
<!--teamMember03 modal -->
<div class='modal fade teamMemberPop' id='teamMember03' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/avnish.png' alt='avnish'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							Avanish earned his MBA degree from Indian Institute of Foreign Trade (IIFT) which regularly ranks among the top 5 business school in the country. Avanish has vast experience in the management and development of Web and Mobile based application. After having worked for top global firms like Cargill, Avanish plans to bring same level of high standard consistency, customer satisfaction and Entrepreneurial approach to provide unique solutions for client`s needs.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember03 modal -->
<!--teamMember04 modal -->
<div class='modal fade teamMemberPop' id='teamMember04' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/shash.png' alt='shash'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							Shash is our super star IT consultant who has over 8 years consulting experience. Shash graduated with first class degree from University of Westminster in 2006.  He specialises in technology consulting, architecture recommendation, recommending technology road maps for firms, designing solutions, stake holder management and more. Shash has various industry recognised certifications like TOGAF 9, MCPD, MCTS, Scrum certification. He has worked with companies like Microsoft, Barclays Investment bank, Bank of America Merrill Lynch. In spare time Shash is pursuing Masters in artificial intelligence from DeMontfort university. Shash also enjoys playing badminton, swimming, working out, pub lunches and mandatory long walks with his wife Jess and two Schnauzer dogs Oscar and Diya.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember04 modal -->
<!--teamMember05 modal -->
<div class='modal fade teamMemberPop' id='teamMember05' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/kashmere.png' alt='kashmere'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							Mr Kashmere gets it done. It doesn`t get simpler than that .His quarter century consulting experience in the areas of defence, finance and education accounts for his leadership in various fields and goodwill among clients like General Electric, Lockheed Martin, Siemens, NASA, The Homeland Security Agency etc. Being our most experienced geek, he supervises several of the company`s largest accounts. He determines the strategic direction company must follow, evaluates new business opportunities and oversees our presence in USA. If that doesn`t keep him busy enough, he is a contributing author to the book 'The e-Business Project Manage' and an adjunct professor of management at Temple University`s Fox School of Business.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember05 modal -->
<!--teamMember06 modal -->
<div class='modal fade teamMemberPop' id='teamMember06' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/vilas.png' alt='vilas'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							There is a high probability that the app underneath your fingers was developed our Geekiest member Vilas. Vilas is one of the first member of the Geeky Works family and an outstanding coder. Vilas is always on top of his game and can often be seen researching the latest trend in the tech industry. He`s addicted to coding in such a big way that it`s a everyday task for us to make sure that he doesn`t forget to eat, sleep and go home. On his time off (which is extremely rare) he loves to read, swim and watch movies.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember06 modal -->
<!--teamMember07 modal -->
<div class='modal fade teamMemberPop' id='teamMember07' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/mahesh.png' alt='mahesh'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							With over 5 years experience in PHP, Mahesh brings wealth of experience and expertise to GeekyWorks. His job at GeekyWorks is to ensure that all projects are delivered on time and budget. What makes Mahesh an exceptional project manager is his uncanny ability to explain the most complicated technology concepts in the most simple language. In his spare time, Mahesh does...well Mahesh continues to code and learn about new concepts. Mahesh is a foodie at heart and only takes time off to enjoy his home cooked food.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember07 modal -->
<!--teamMember08 modal -->
<div class='modal fade teamMemberPop' id='teamMember08' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/asiya.png' alt='asiya'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							With strong attention to detail and an overall passion for creating innovative android apps, Asiya heads the Android App development division and enjoys it thoroughly. Asiya is a hard worker and is often the last one to leave the office. She is a perfectionist and can often be seen fixing stuff that might not even qualify as an issue. She is an excellent cook and loves to treat everyone with her delicacies.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember08 modal -->
<!--teamMember09 modal -->
<div class='modal fade teamMemberPop' id='teamMember09' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
        	<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/ravi.png' alt='ravi'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							Ravi is the latest member to join the Geek family. Ravi started off at Geeky Works as an intern on January 2nd and soon proved his worth. Bitten by the coding bug during the very first days of his college, Ravi spent his next few years relentlessly writing and perfecting his code. Ravi has a passion for creating unique and challenging iOS applications, with clean and efficient code. He is big time into EDM music and occasionally takes an off to attend Hardwell and Avici concerts.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>

    </div>
  </div>
</div>
<!--teamMember09 modal -->
<!--teamMember10 modal -->
<div class='modal fade teamMemberPop' id='teamMember10' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
    	<div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
      </div>
      <div class='modal-body'>
        <div class='teamMemberDetails'>
					<div class='col-sm-4'>
						<div class='memberPic'>
							<img src='images/geeky/aparna.png' alt='aparna'/>
						</div>
					</div>
					<div class='col-sm-8'>
						<div class='memberDesc'>
							Aparna is responsible for testing the code that developers write to ensure that it is bug free and runs smoothly. She has a natural curiousity to learn and believes in staying ahead of the curve. During her last few months at GeekyWorks, she has become an indispensable part of the team. She considers shopping Malls to be her second home and thoroughly believes in the concept of shop-till-you-drop. Any remaining time is usually spent dancing, eating chocolates, playing games on iPad, painting and trekking.
						</div>
					</div>
				</div>
      </div>
      <div class='modal-footer'>
      </div>
    </div>
  </div>
</div>
<!--teamMember10 modal -->