<div class='headerWrapper'>
<!--award-->
<div class='awardContainer'>
<div class='award text-center'>
<a href='http://mobile.siliconindia.com/company/Geeky-Works-catid-25-cid-535.html' target='_blank'>Silicon India ranks Geeky Works among the Top 50 App Development firms in India.</a>
</div>
</div>
<!--award close-->
  <!-- Header -->
  <nav class='navbar-default headerContainer'>
    <div class='container header'>
    <!-- Brand and toggle get grouped for better mobile display -->
      <div class='row'>
        <div class='col-sm-4 col-xs-12'>
          <div class='navbar-header'>
            <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
              <span class='sr-only'>Toggle navigation</span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
            </button>
            <a class='logo' href='index.php'>
          <img src='images/geekyworks_logo.jpg' alt='Geekyworks Logo' />
          </a>
          </div>
        </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
        <div class='col-sm-8 col-xs-12'>
          <div class='collapse navbar-collapse myNav' id='bs-example-navbar-collapse-1'>
            <ul class='nav navbar-nav navbar-right'>
              <li><a class='scrollInner' href='index.php#ourServices'>Services</a></li>
              <li><a class='scrollInner' href='index.php#ourWork'>Work</a></li>
              <li><a class='scrollInner' href='index.php#ourTeam'>Team</a></li>
              <li><a href='/blog/'>Blog</a></li>
              <li><a href='media.php'>Media</a></li>
              <li><a href='career.php'>Career</a></li>
              <li><a class='scrollInner' href='index.php#contact'>Contact</a></li>
            </ul>
          </div>
        </div>
      <!--navbar-collapse -->
      </div>
    </div>
  <!-- container-fluid -->
  </nav>
<!-- Header -->
</div>
