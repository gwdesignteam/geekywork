<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
    <div id='wrapper'>
      <?php include ('headerPage.php');?>
    </div>
    <!--work container-->
     <div class='workHeaderContainer'>
        <div class='container'>
          <div class='workHeader'>
            Health Assist
            <h1>IOS and Android App</h1>
          </div>
        </div>
      </div>
      <div class='aboutWorkBox'>
        <div class='container'>
          <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
              <img src='images/health-assist.png' alt='Health Assist'/>
          </div>
          <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
            <div class='workDiscriptionTitle'>
              <strong>CLIENT</strong>
              <br />
              WellCare Today, LLC
            </div>
            <br /><br/>
            <p>Health  Assist is an application designed to help people who take prescription medication, better manage their condition. As a personal health assistant, the app provides you with condition-related reminders, information, savings and access to support services, thereby improving your health. 
            <br/><br />
            This app comes with a plethora of features like:
            <br /><br/>
            <ul class='weHave'>
              <li>Security: Encryption and decryption</li>
              <li>Push Notifications based medication reminders</li>
              <li>Medication and refill (medication) alerts</li>
              <li>Facility for custom reminder configuration</li>
              <li>Automatic Adherence Alert</li>
              <li>Text-Speech-Text enabled service integration</li>
              <li>Weather, Horoscope and Stock exchange integration</li>
            </ul></p><br />
            <p>You can download it here</p>
            <a href='https://play.google.com/store/apps/details?id=com.org.healthassist&hl=en' target='_blank'><img src='images/google_play_icon.jpg' alt='Google Play'/></a>
            <a href='https://itunes.apple.com/us/app/healthassist/id351634824?mt=8' target='_blank'><img src='images/app-store.jpg ' alt='App Store' /></a>
            <br /><br />
          </div>
        </div>
        </div>
    <a class='proNav proPrev' href='upme.php' title='Previous'></a>
    <a class='proNav proNext' href='mohar.php' title='Next'></a>
    <!-- Last Text Note -->
    <?php include ('footer.php');?>
    <!-- Last Text Note -->
    <?php //include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
  </body>
</html>