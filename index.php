<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta property='og:image' content='http://geekyworks.com/images/gw_logo.jpg' />
		<meta name='title' content='Award Winning Web and Mobile Application development company'/>
		<meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
		<meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
		<title>Award Winning Web and Mobile Application development company</title>
		<link rel='shortcut icon' href='/favicon.ico' type='image/x-icon'>
		<link rel='icon' href='/favicon.ico' type='image/x-icon'>
		<?php include ('assetCss.php');?>
	</head>
	<body>
		<?php include ('header.php');?>
		<!--welcome text-->
		<div class='welcomeContainer' id='welcome'>
			<div class='container'>
				<div class='row'>
					<div class='welcomeTextWrapp'>
						<div class='welcomeText'>
							<div class='welcomeTextInner'> hi. we are geekyworks.
								and you are
								<input type='text' class='welcomeInputField' id='textOne' onkeyup='displayname()' placeholder=' enter your name' />
							</div>
						</div>
						<!--welcome text end-->
					</div>
				</div>
			</div>
		</div>
		<!--welcome text second-->
		<div class='welcomeContainerSecond'>
			<div class='welcomeTextSecond container'>
				<div class='welcomeTextSecondHeader col-sm-8 col-sm-offset-2'> hey
						<input type='text' id='textTwo'/>
						, it’s great to meet you !
				</div>
				<div class='col-sm-8 col-sm-offset-2'>
					we’re a bunch of tech junkies who create user oriented digital experiences. we work with the accuracy & precision that can make even the Swiss look disorganized. we love deadlines, great ideas & video games_
				</div>
			</div>
		</div>
		<!--welcome text second end-->
		<!--Our Work-->
		<div id='ourWork' class='workContainer'>
			<div class='work container'>
				<h2 class='col-sm-12 text-center'>here’s some of our work_</h2>
				<a href='tata-bss.php' class='workBox col-sm-4 col-xs-6'>
					<img src='images/ourWork/tata-Mac-Screen.png' alt='Tata bss website'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='toto.php'>
					<img src='images/ourWork/totoSmall.png' alt='Toto ios app'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='reliance.php'>
					<img src='images/ourWork/Reliance.jpg' alt='Reliance ios app'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='BookMyCab.php'>
					<img src='images/ourWork/BookmyCab-Phone-Screen.jpg' alt='Book my cab android app'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='Golfingindian.php'>
					<img src='images/ourWork/Golfingindian-Mac-Screen.jpg' alt='Golfingindian website'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='upme.php'>
					<img src='images/ourWork/UPME-Phone-Screen.jpg' alt='Upme ios and android app'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='healtAssist.php'>
					<img src='images/ourWork/health-assist.png' alt='Health assist ios and android app'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='mohar.php'>
					<img src='images/ourWork/mohar-Mac-Screen.png' alt='Mohar garima website'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='quinta.php'>
					<img src='images/ourWork/quinta-Mac-Screen.png' alt='La quinta website'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='storekaro.php'>
					<img src='images/ourWork/storekaro-small.png' alt='Storekaro website'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='kul.php'>
					<img src='images/ourWork/kulSmall.png' alt='Kumar Builders (KUL)'/>
				</a>
				<a class='workBox col-sm-4 col-xs-6' href='meta-arch.php'>
					<img src='images/ourWork/metaSmall.png' alt='Meta Arch'/>
				</a>
			</div>
		</div>
		<!--Our Work end-->
		<!--Testimonials-->
		<div class='container'>
			<div class='testimonialsWrapp'>
					<h2 class='col-sm-12 text-center'>testimonials_</h2>
					<br />
					<br />
					<div class='testimonialSliderWrapp'>
							<div class='slide'>
									<div class='testimonialSlider'>
										<!--Testimonials 1-->
										<div id='1'>
											<div class='testimonialsBox'>
												<div class='testimonialsContent'>When we asked Geeky Works to work on our mobile and desktop site, we were a little skeptical about outsourcing such an important task to India. Geeky Works surpassed our expectations at each and every stage. This was not only due to great technical skills but also excellent communication, development and project management. </div>
												<div class='testimonialsBoxArrow'></div>
												<div class='testimonialsImg'><img src='images/testimonial/2014-04-25_1928.png' alt='Matthew, Nexzest Owner'/></div>
												<div class='testimonialsName'>Matthew Arata, US<br />
												<span style='font-size:15px;'>Nexzest Owner</span> </div>
											</div>
										</div>
										<!--Testimonials 1 end-->
										<!--Testimonials 2-->
										<div id='3'>
											<div class='testimonialsBox'>
												<div class='testimonialsContent'>I'm still amazed at the quality of work of this team. The interaction was smooth, the work was fast, and the result was very professional. The experience was so pleasant that I will work again with Geeky Works. I earned a broader view of how an app is developed. You guys are really great at what you do.</div>
												<div class='testimonialsBoxArrow'></div>
												<div class='testimonialsImg'><img src='images/testimonial/198727_581088354980_6940639_n[1].jpg' alt='Jessica, Pictomatic App Owner'/></div>
												<div class='testimonialsName'>Jessica Robinson, UK <br />
												<span style='font-size:15px;'>Pictomatic App Owner</span></div>
											</div>
										</div>
										<!--Testimonials 2 end-->
										<!--Testimonials 3-->
										<div id='4'>
											<div class='testimonialsBox'>
												<div class='testimonialsContent'>We absolutely love Geeky Works and how they are like an extension of our team. They exceeded our expectations each and every time. They designed a great looking User Interface for our Android App. Their team is friendly and very responsive. We would highly recommend Geeky Works. </div>
												<div class='testimonialsBoxArrow'></div>
												<div class='testimonialsImg'><img src='images/testimonial/2014-10-16_1633.png' alt='Rajat, BookMyCab app owner'/></div>
												<div class='testimonialsName'>Rajat Deshpande<br />
												BookMyCab </div>
											</div>
										</div>
										<!--Testimonials 3 end-->
									</div>
							</div>
					</div>
			</div>
		</div>
		<!--Testimonials end-->
		<!--Our Team-->
		<div class='ourTeamWrapp' id='ourTeam'>
			<div class='container'>
				<h2 class='col-sm-12 text-center'>and here’s our team of geeks_</h2>
				<div class='teamMembersWrapp'>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember01'>
						<img src='images/geeky/anant.png' alt='Anant'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember02'>
						<img src='images/geeky/satyendra.png' alt='Satyendra'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember03'>
						<img src='images/geeky/avnish.png' alt='Avanish'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember04'>
						<img src='images/geeky/shash.png' alt='Shash'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember05'>
						<img src='images/geeky/kashmere.png' alt='Kashmere'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember06'>
						<img src='images/geeky/vilas.png' alt='Vilas'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember07'>
						<img src='images/geeky/mahesh.png' alt='Mahesh'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember08'>
						<img src='images/geeky/asiya.png' alt='Asiya'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember09'>
						<img src='images/geeky/ravi.png' alt='Ravi'/>
					</div>
					<div class='col-sm-4 teamMember' data-toggle='modal' data-target='#teamMember10'>
						<img src='images/geeky/aparna.png' alt='Aparna'/>
					</div>
				</div>
				<div class='clearfix'></div>
				<div class='ourTeamText col-sm-8 col-sm-offset-2'>
				Our geeks don’t stop working until they get a ‘yes-this-is-what-I-wanted’ smile from you. It’s not everyday you find nerds so tech-obsessed that they hit Ctrl+F to fetch themselves a coffee sometimes. They refuse to settle on anything less than extraordinary_
				</div>
			</div>
		</div>
		<!--Our Team end-->
		<!--More Text-->
		<div class='enoughTextWrapp'>
			<div class='container'>
				<div class='col-sm-8 col-sm-offset-2 welcomeTextThrird'>enough about us<br />
					so <input type='text' id='textThree'/>
					, what can we do for you today?
				</div>
			</div>
		</div>
		<!--More Text end-->
		<!--Services-->
		<div class='servicesWrapp' id='ourServices'>
			<div class='container'>
				<div class='col-sm-6 servicesIphone'>
					<div class='basic' data-toggle='modal' data-target='#androidIos'>
						<h1 class='servicesHeader'> iPHONE & ANDROID APPLICATIONS </h1>
						<div class='servicesIcon'>
							<img src='images/iphone_app_icon.jpg' width='190px' height='195px' alt='iPHONE & ANDROID APPLICATIONS'/>
						</div>
						<div class='serviceDesc'>Whether you want to take your business, website or concept to the app store, or just want to develop an exciting app, our app developers can help you with the job.
						</div>
					</div>
				</div>
				<div class='col-sm-6'>
				<div class='basic' data-toggle='modal' data-target='#webDesignDev'>
					<h1 class='servicesHeader'> WEBSITE DESIGN & DEVELOPMENT </h1>
					<div class='servicesIcon'>
						<img src='images/web_design_icon.jpg' width='290px' height='195px' alt='WEBSITE DESIGN & DEVELOPMENT'/>
					</div>
					<div class='serviceDesc'>We specialize in developing high quality, Content Management System (CMS) powered websites for our clients.
					</div>
				</div>
				</div>
			</div>
		</div>
		<!--Services end-->
		<div class='clearfix'></div>
		<!--More Text-->
		<div class='whatWeThinkMsgWrapp'>
			<div class='whatWeThinkMsg'>
				<div class='container'>
					<div class='col-sm-8 col-sm-offset-2'>
						at geekyworks, we make your ideas talk, walk, dance & fly. If you are looking to just get the work done, then we’re not really looking for you. But if ground breaking solutions make your heart pump faster, our wavelengths surely match. coffee shops, tennis lawns, breakfast tables, or your office – we’re open to discussions anywhere. Just give us a call or drop us a message below and we’ll be there_
					</div>
				</div>
			</div>
			<a id='contact'></a>
			<div class='whatWeThinkMsgBottom'></div>
		</div>
		<!--More Text end-->
		<!--Contact Form-->
		<div class='contactFormWrapp'>
			<div class='col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2'>
				<form action='contact.php' method='post' onsubmit="return validateFrm();">
					<div class='col-sm-12'>
						<input type='text' name='name'  value='name_' onFocus="if (value == 'name_') {value=''}" onBlur="if (value== '') {value='name_'}" class="contactNameTextfield" required="required"  />
						<span class='error_text'><?php //echo $error_message;?></span>
					</div>
					<div class='col-md-6 col-sm-12'>
						<input type='text' name='email'  value='e-mail id_' onFocus="if (value == 'e-mail id_') {value=''}" onBlur="if (value== '') {value='e-mail id_'}" class="contactEmailTextfield" required="required"  />
						<span class='error_text'><?php //echo $error_message;?></span>
					</div>
					<div class='col-md-6 col-sm-12'>
						<input type='text' name='telephone'  value='phone number_' onFocus="if (value == 'phone number_') {value=''}" onBlur="if (value== '') {value='phone number_'}" class="contactPhoneTextfield" required="required"  />
						<span class='error_text'><?php //echo $error_message;?></span> <br />
					</div>
					<div class='col-sm-12'>
						<textarea name='message' required='required' class='contactFormTextarea' onfocus="if(this.value==this.defaultValue)this.value=''" onblur="if(this.value=='')this.value=this.defaultValue">feel free to drop us a message_</textarea>
						<span class='error_text'><?php //echo $error_message;?></span>
					</div>
					<div class='col-sm-12'>
						<input type='submit' value='' class='submit_button'  />
					</div>
				</form>
				<div class='clearfix'></div>
			</div>
		</div>
		<!--Contact Form end-->
		<!--Last Text Note-->
		<?php include ('footer.php');?>
		<!--Last Text Note end-->
		<!--androidIos modal-->
		<div class='modal fade' id='androidIos' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
		  <div class='modal-dialog' role='document'>
		    <div class='modal-content'>
		    	<div class='modal-header'>
		        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
		      </div>
		      <div class='modal-body'>
		        <div class='androidIosWrapp'>
							<div class='col-sm-6 serviceIcon' onclick="window.location = 'iphone-dev.php';">
								<div class='iosLink'>
									<img src='images/Apple-Logo.png' alt='iPHONE APPLICATIONS'/> <br />
									<br />
									<div class='servicesHeader' > iPHONE	APPLICATIONS </div>
								</div>
							</div>
							<div class='col-sm-6 serviceIcon' onclick="window.location = 'android-dev.php';">
								<div class='androidLink'>
									<img src='images/Android.png' alt='ANDROID APPLICATIONS'/> <br />
									<br />
									<div class='servicesHeader' > ANDROID APPLICATIONS </div>
								</div>
							</div>
						</div>
		      </div>
		      <div class='modal-footer'></div>
		    </div>
		  </div>
		</div>
		<!--androidIos modal end-->
		<!--webDesignDev modal-->
		<div class='modal fade' id='webDesignDev' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
			<div class='modal-dialog' role='document'>
				<div class='modal-content'>
					<div class='modal-header'>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
					</div>
					<div class='modal-body'>
						<div class='androidIosWrapp'>
							<div class='col-sm-6 serviceIcon' onclick="window.location = 'web-design.php';" >
								<div class='iosLink'>
									<img src='images/web_design_icon.png' alt='WEBSITE DESIGN'/> <br />
									<br />
									<div class='servicesHeader'> WEBSITE DESIGN </div>
								</div>
							</div>
							<div class='col-sm-6 serviceIcon' onclick="window.location = 'web-dev.php';">
								<div class='androidLink'>
									<img src='images/Web_development.png' alt='WEBSITE DEVELOPMENT'/> <br />
									<br />
									<div class='servicesHeader'> WEBSITE DEVELOPMENT </div>
								</div>
							</div>
						</div>
					</div>
				<div class='modal-footer'>
				</div>
				</div>
			</div>
		</div>
		<!--webDesignDev modal end-->
		<div id='start_popup' style='display:none;' class='popup-one'> <img src='images/startup_img.png' alt='start_img' alt='Geekyworks'/> </div>
		<?php include ('assetJs.php');?>
	</body>
</html>
