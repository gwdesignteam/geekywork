function displayname () {
	var name = document.getElementById('textOne').value;
	document.getElementById('textTwo').value=name;
	document.getElementById('textThree').value=name;
	if(document.getElementById('textTwo').value=name){
		document.getElementById('textTwo').style['display'] = 'inline-block';
		document.getElementById('textTwo').size = document.getElementById('textTwo').value.length;
	}else{
		document.getElementById('textTwo').style['display'] = 'none';
	}
	if(document.getElementById('textThree').value=name){
		document.getElementById('textThree').style['display'] = 'inline-block';
		document.getElementById('textThree').size = document.getElementById('textThree').value.length;
	}else{
		document.getElementById('textThree').style['display'] = 'none';
	}
	if(document.getElementById('textFour').value=name){
		document.getElementById('textFour').style['display'] = 'inline-block';
		document.getElementById('textFour').size = document.getElementById('textFour').value.length;
	}else{
		document.getElementById('textFour').style['display'] = 'none';
	}
}
//select menu item from the menu and hide toggle in mobile
$(document).on('click','.navbar-collapse.in',function(e) {
    if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
        $(this).collapse('hide');
    }
});
//click away the menu and hide toggle in mobile
$(document).on('click',function(){
$('.collapse').collapse('hide');
})
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$('.testimonialSlider').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: 'unslick'
			// instead of a settings object
		]
	});
	var x = $('.navbar-header').height();
  $('.myNav').css('height', x);
  $('.myNav').css('position', 'relative');
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1){
			$('.headerWrapper').addClass('sticky');
			}
			else{
			// console.log(98899);
			$('.headerWrapper').removeClass('sticky');
		}
	});
});
var timeOut;
function scrollToTop() {
	if (document.body.scrollTop!=0 || document.documentElement.scrollTop!=0){
		window.scrollBy(0,-50);
		timeOut=setTimeout('scrollToTop()',10);
	}
	else clearTimeout(timeOut);
}
// function for arrows in client pages
$(window).scroll(function () {
	if ($(window).scrollTop() >= 300) {
		$('.proNav').addClass('proNavAct');
	}
	else{
		$('.proNav').removeClass('proNavAct');
	}
});
