<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
    <div id='wrapper'>
      <?php include ('headerPage.php');?>
    </div>
    <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'> KUL Android App
        <h1>Web Design & Development</h1>
        </div>
      </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'>  
          <img src='images/kul.png' alt='Kumar Builders'/> 
        </div>
      <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
        <div class='workDiscriptionTitle'> <strong>CLIENT</strong> <br />
          Kumar Builders (KUL) 
          </div>
          <br />
          <p>With a legacy spanning over 49 years, KUL is one of the most reputed real estate builder in Pune. KUL operates in a highly competitive market and was looking to establish their presence on mobile through an android based mobile application. With the help of this app, they wanted to enable their target audience to view the details of the properties that were available for purchase.<br />
          <br />
          During the initial discussion with the client, the idea was at a very nascent stage and the client expected us to brainstorm and suggest them the features that would make the application highly useful and facilitate the process of house hunting. <br />
          <br />
          After a lot of brainstorming sessions we built an application that came equipped with the following features: <br />
          <br />
          <ul class='weHave'>
            <li>It allowed the user to view the available properties on a Google map</li>
            <li>Helped the prospect to directly have a conversation with the sales team via chatting functionality</li>
            <li>Allowed the app users to favorite and share that property via social media platforms like twitter and facebook</li>
            <li>A special functionality that allowed the prospect to customize their favorite property and design it as per their own liking.</li>
          </ul>
          </p>
          <br />
          <p>
          <p>You can download it here</p>
          <a href='https://play.google.com/store/apps/details?id=com.kumarbuilders.propertyapp' target='_blank'><img src='images/google_play_icon.jpg' alt='Google Play'></a><br />
          <br />
          <br />
        </div>
      </div>
    </div>
    <a class='proNav proPrev' href='storekaro.php' title='Previous'></a>
    <a class='proNav proNext' href='meta-arch.php' title='Next'></a>
    <!-- Last Text Note -->
    <?php include ('footer.php');?>
    <!-- Last Text Note -->
    <?php //include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
  </body>
</html>