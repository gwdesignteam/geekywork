<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
  <div id='wrapper'>
    <?php include ('headerPage.php');?>
  </div>
  <!--work container-->
<!--Header-->
  <!--media start-->
  <div class='media'>
    <div class='title container'><p>Awards and Recognition</p></div>
    <div class='mediaContainer'>
      <div class='container'>
        <div class='awardList col-sm-6 col-xs-12'>
          <div class='awardTitle'>Geeky Works was named the 'The Most Trusted Digital Media Solution Provider for the Year 2014-15'.</div>
          <div class='pic'>
            <video width='320' height='240' controls>
              <source src='Award.mp4' type='video/mp4'>
              <source src='Award.ogg' type='video/ogg'>
              Your browser does not support the video tag.
            </video>
          </div>
          <div class='awardDetails'><p>Geeky Works was named the 'The Most Trusted Digital Media Solution Provider for the Year 2014-15' by All India Economic Survey Award Council (AIESAC), an Indian Government Organization.</p></div>
        </div>
        <div class='awardList col-sm-6 col-xs-12'>
          <div class='awardTitle'>Silicon India ranks Geeky Works among the Top 50 Mobile App Development Firms in India.</div>
          <div class='pic'><img src='images/award-2.png' alt='Award' /></div>
          <div class='award-details'><p>Geeky Works powered its way to the Top 50 App Development Firm in India. It's a great achievement for the Geeks and we just hope to keep on getting better with time. <a href='http://mobile.siliconindia.com/company/Geeky-Works-catid-25-cid-535.html' target='_blank'>Read more</a></p></div>
        </div>
      </div>
    </div>
  </div><!--media end-->
  <!--Last Text Note-->
  <?php include ('footer.php');?>
  <!--Last Text Note-->
  <?php //include ('assetPageJs.php');?>
  <?php include ('assetJs.php');?>
</body>
</html>