<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
    <div id='wrapper'>
      <?php include ('headerPage.php');?>
    </div>
    <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'> META ARCH
        <!--<h1>iPad App</h1>-->
        </div>
      </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'>  
          <img src='images/MetPadScreen_lg.jpg' alt='Meta Pad Screen' /> 
        </div>
        <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'> <strong>CLIENT</strong> <br />
          META ARCH </div>
          <br />
          <p>One fine day, one of our Geeks got a call from a client who had finally decided to put an end to the 
          attendance management chaos. With more than 500 employees working for his firm, keeping track 
          of the attendance, salary, leaves etc. manually had turned into a headache for the HR, Finance and 
          the admin departments.</p>
          <br />
          <p>The problem was genuine and we were excited about finding a solution. Our software provides real-time visibility to measure project , manage labours for increased productivity, efficiency and labour performance. 
          We designed the Software to provide integrated solution by bringing Finance, HR and all the other 
          divisions on to a single platform. The labour management system is being powered by a mobile app 
          which enables the supervisor to take more than 500 attendances within no time by scanning QR card 
          assigned to each employee.</p>
          <h3>The software comes with a plethora of features like:</h3>
          <ul class='weHaveList'>
            <li>Project details and project status</li>
            <li>Enterprise Reports and Data Analysis.</li>
            <li>Labour Attendance.</li>
            <li>Create task list and Assign task.</li>
            <li>Allowances and Performance rating.</li>
            <li>Request for required/release resource.</li>
            <li>Shift management ,Time Tracking.</li>
          </ul>
          <br />
          <p>Our client was happy and our Geeks celebrated the success with cold beer!!</p>
          <br />
          <p><em>This is an Enterprise Application and is only available for organization's internal use. Please get in touch with us to request a demo.</em></p>
          <br />
          <!--<p>You can download it here</p>
          <a href='https://itunes.apple.com/in/app/toto-india/id783506846?mt=8' target='_blank'><img src='images/app-store.jpg' /></a>--> 
        </div>
      <a class='proNav proPrev' href='kul.php' title='Previous'></a>
      <a class='proNav proNext' href='tata-bss.php' title='Next'></a>
      </div>
    </div>
    <!-- Last Text Note -->
    <?php include ('footer.php');?>
    <!-- Last Text Note -->
    <?php include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
  </body>
</html>
