<html lang='en'>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
  <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
  <title>.:: Our Works ::.</title>
  <?php include ('assetCss.php');?>
</head>
  <body>
    <div id='wrapper'>
    <?php include ('headerPage.php');?>
    </div>
    <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'> Mohar.co.in
          <h1>Web Design & Development</h1>
        </div>
      </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
          <img src='images/mohar.png' alt='Mohar' /> 
        </div>
        <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'> <strong>CLIENT</strong> <br />
          Mohar Garima </div>
          <br />
          <p>Mohar Garima is a subsidiary of Rohan Builders and it specializes in developing high-end, super luxurious apartments that start from the range of almost USD 2 million. Website is the first impression that a potential client forms of a business so it was extremely important that the site conveys the same level of sophistication as the apartments. We held quite a few meetings with the client before officially kicking off the project to get a very clear understanding of the image that they wanted to portray through their website. Finally, we put together a great looking, user friendly website for one of our favorite client. A site that matched their brand standards and urged the user to explore more. </p>
          <br />
          <p>You can visit the website at:</p>
          <p class='projectUrl'><a href='http://www.mohar.co.in/' target='_blank'>www.mohar.co.in</a></p>
        </div>
      </div>
    </div>
    <a class='proNav proPrev' href='healtAssist.php' title='Previous'></a>
    <a class='proNav proNext' href='quinta.php' title='Next'></a>
    <!-- Last Text Note -->
    <?php include ('footer.php');?>
    <!-- /Last Text Note -->
    <?php //include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
  </body>
</html>