<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
    <div id='wrapper'>
    <?php include ('headerPage.php');?>
    </div>
    <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'> La quinta
          <h1>Web Design & Development</h1>
        </div>
      </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
          <img src='images/quinta.png' alt='Quinta' /> 
        </div>
        <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'> <strong>CLIENT</strong> <br />
           La Quinta </div>
          <br />
          <p>La Quinta Inn is a chain of limited service hotels in US, Canada and Mexico. The company owns and operates over 700 properties and franchises so it was a real honor when we got an opportunity to work with one of the hotel based in Glen Burnie. La Quinta Glen Burnie required a new web presence through the development of a brand new website. They also wanted to spread the word about the hotel using Search Engine Optimization services. After a couple of chats, La Quinta Glen Burnie entrusted their faith on us and decided to leave IT to the Geeks. <br />
          Our Geeks took good care of the website and the marketing activities. We did not have much freedom to unleash our creativity since the site had to follow certain guidelines set by La Quinta Company. We decided to keep things to the minimal on their new website while making sure that it looked clean and professional. All the information that really mattered to the user was well organized and presented immediately as the website opened. <br />
          Our geeks also took good care of the marketing activities and made sure that the website successfully ranked for the keywords that people would use to find a hotel in Glen Burnie online.</p>
          <br />
          <p>
          <p>You can visit the website at:</p>
          <p class='projectUrl'><a href='http://www.laquintabaltimoreglenburnie.com/' target='_blank' >www.laquintabaltimoreglenburnie.com</a></p>
          <br />
          <br />
        </div>
        </div>
      </div> 
      <a class='proNav proPrev' href='mohar.php' title='Previous'></a>
      <a class='proNav proNext' href='storekaro.php' title='Next'></a>
      <!--Last Text Note-->
      <?php include ('footer.php');?>
      <!--Last Text Note-->
      <?php //include ('assetPageJs.php');?>
      <?php include ('assetJs.php');?>
    </body>
</html>
