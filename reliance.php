<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
    <div id='wrapper'>
      <?php include ('headerPage.php');?>
    </div>
    <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'>
          Reliance Energy
        <h1>IOS App</h1>
        </div>
      </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
          <img src='images/Reliance_big.png' alt='Reliance' />
        </div>
        <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'>
            <strong>CLIENT</strong>
            <br />
            Reliance Energy
          </div>
          <br />
          <p>This app is an initiative by Reliance Energy to help consumers keep a track of their electricity consumption. The app comes with  multitude of cool features like:
          'Current Consumption' -  This allows the user to view their electricity consumption in daily, weekly or monthly format.
          'My Cost Status'  - Allows the user to calculate the bill that has been generated so far due to electricity consumption. 
          'My Alerts' - User gets a notification if the server detects any unusual occurence in the electricity consumption. 
          'Saving Tips' - As the name suggests, app frequently offers highly useful tips to save electricity and consume effectively. 
          This is a fairly comprehensive app and the client plans to add more useful features in the near future. Stay tuned for a highly advanced application that aims to make your life so much more easier.</p>
          <br />
      </div>
    </div>
    </div>
    <a class='proNav proPrev' href='toto.php' title='Previous'></a>
    <a class='proNav proNext' href='BookMyCab.php' title='Next'></a>
    <!--Last Text Note-->
    <?php include ('footer.php');?>
    <!--Last Text Note-->
    <?php //include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
  </body>
</html>