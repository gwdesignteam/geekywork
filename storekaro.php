<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
    <div id='wrapper'>
      <?php include ('headerPage.php');?>
    </div>
    <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
          <div class='workHeader'> Storekaro.com
            <h1>Web Design & Development</h1>
          </div>
        </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'>  
          <img src='images/storekaro.png' />
        </div>
        <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'> <strong>CLIENT</strong> <br />
          Storekaro 
          </div>
          <br />
          <p>Storekaro.com is the one stop-solution for all your storage needs. From household goods and office
          furniture to motorbikes and cars, you can store it all. Storekaro.com is a unique concept that allows 
          you to do everything online with just the click of a button. With CCTV cameras, 24 hours security, 
          fire alarm systems and smoke detectors, Storekaro.com plans to provide you complete peace of 
          mind by ensuring that your good is safe at every moment.<br />
          <br />
          Self-Storage service is a relatively new concept in India and the client wanted us to effectively 
          educate the users about the idea and then make them comfortable about paying for the service 
          online.<br />
          <br />
          Website is the first impression that your client forms of your business and it was important to make
          sure that the site looked trustworthy. It was not just about making a good impression on the visitor 
          but also about educating him and making him comfortable with the whole concept.<br />
          <br />
          We realized that User Experience would play a very crucial part in making the website successful and
          took the time to understand and analyze what the customer would want to see on such a website.<br />
          <br />
          We carried out an in-depth analysis of what the current competitors were offering and the target
          audience's perception about the service. The information resulting from the research was then 
          applied while designing the website. Housing a lot of content, while keeping the experience 
          pleasurable is what made the project so much fun.<br />
          <br />
          Storekaro.com now has a state-of-art website that offers a complete, secure and friendly online 
          storage experience to its users.</p>
          <br />
          <p>You can visit the website at:</p>
          <p class='projectUrl'>
          <a href='http://storekaro.com/' target='_blank'>www.storekaro.com</a></p>
          <br />
          <br />
        </div>
      </div>
    </div>
    <a class='proNav proPrev' href='quinta.php' title='Previous'></a>
    <a class='proNav proNext' href='kul.php' title='Next'></a>
      <!--Last Text Note-->
    <?php include ('footer.php');?>
    <!--Last Text Note-->
    <?php //include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
  </body>
</html>