<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
  <div id='wrapper'>
    <?php include ('headerPage.php');?>
  </div>
  <!--work container-->
  <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'> TATA BSS
          <h1>Web Design & Development</h1>
        </div>
      </div>
  </div>
  <div class='aboutWorkBox'>
    <div class='container'>
      <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
        <img src='images/tata.png' alt='Tata BSS'/> 
      </div>
      <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'>
            <strong>CLIENT</strong> 
            <br />
            Tata BSS 
          </div>
          <br />
          <p>Tata Business Support Services is a wholly owned subsidiary of Tata Sons, the holding company of India's largest business group, Tata. Tata-BSS manages a customer community of 600 million members every day. <br />
          <br />
          The company approached us to take over the development of their web presence. We were expected to deliver a website that is modern, user friendly, leads to more sales and is driven by innovation - part of company's DNA. <br />
          <br />
          Before starting any design work we held a few meetings with the client to fully understand their requirement. We browsed through almost all Tata group websites to analyze and understand the thread of common element that Tata websites carry. <br />
          <br />
          Given the diverse nature of services offered by the group, additional emphasis was put on designing a navigation plan that was intuitive and well structured. <br />
          <br />
          With a pen, paper and a cup of coffee, we first conceptualized the overall look and feel of the website. Only once everything was mapped out did we start building the wireframes. <br />
          <br />
          The result was a beautiful responsive website with intuitive navigation. The website was very well received by the Tata and its customers. Our Geeks were happy to have worked on and successfully deliver a website for the biggest business group in India. <br />
          <br />
          So when it comes to technology even Tata 'Leaves IT to the Geeks'. <br />
          <br />
          Have a look at the website and let us know how did you like the work? </p>
          <br />
          <p>The website is live now; you can visit it at:</p>
          <p class='projectUrl'>
            <a href='http://tata-bss.com/us/' target='_blank'>www.tata-bss.com/us</a>
          </p>
          <br />
          <br />
      </div>
    </div>  
  </div>
  <a class='proNav proPrev' href='meta-arch.php' title='Previous'></a>
  <a class='proNav proNext' href='toto.php' title='Next'></a>
  <!--Last Text Note-->
  <?php include ('footer.php');?>
  <!--Last Text Note-->
  <?php //include ('assetPageJs.php');?>
  <?php include ('assetJs.php');?>
  </body>
</html>