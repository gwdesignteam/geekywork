<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
  <div id='wrapper'>
    <?php include ('headerPage.php');?>
  </div>
  <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'> TOTO
          <h1> iPad App</h1>
        </div>
      </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
          <img src='images/TOTO-Pad-Screen_lg.jpg' alt='Toto Pad Screen' /> 
        </div>
        <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'> <strong>CLIENT</strong> <br />
            TOTO 
          </div>
          <br />
          <p>TOTO Ltd. was founded in  the year 1917. It is based in Kitakyushu, Japan and is World's largest toilet manufacturer. After evaluating all the major App Development companies in Mumbai, TOTO decided to work with us. TOTO basically wanted a catalogue app for iOS platform, that can act as a powerful marketing tool for its Sales team. TOTO is known to develop the World's most advanced, effecient (and expensive :)) toilets in World. Looking at the reputation that TOTO carries, it wasn't certainly easy for our team to brainstorm the app details that can compliment the company's stature. After quite a few round of discussions and long meetings, we finally started the work and the rest is history friends. TOTO loves our app and our Geeks! TOTO's app does a great job in presenting their state-of-art products in a very beautiful way. </p>
          <br />
          <p>We soon plan to start the work on Android version of the app. Their application is available on the App Store. </p>
          <br />
          <p>You can download it here</p>
          <a href='https://itunes.apple.com/in/app/toto-india/id783506846?mt=8' target='_blank'><img src='images/app-store.jpg' /></a>
          <br />
          <br />
        </div>
      </div>
    </div>
  <a class='proNav proPrev' href='tata-bss.php' title='Previous'></a>
  <a class='proNav proNext' href='reliance.php' title='Next'></a>
  <!-- Last Text Note -->
  <?php include ('footer.php');?>
  <!-- Last Text Note -->
  <?php //include ('assetPageJs.php');?>
  <?php include ('assetJs.php');?>
</body>
</html>