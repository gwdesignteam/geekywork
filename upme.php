<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Our Works ::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
  <div id='wrapper'>
    <?php include ('headerPage.php');?>
  </div>
  <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <div class='workHeader'> UPME Social
          <h1>IOS & Android App</h1>
        </div>
      </div>
    </div>
    <div class='aboutWorkBox'>
      <div class='container'>
        <div class='projectThumbnail col-md-8 col-sm-12 col-xs-12'> 
          <img src='images/UPME-Phone-Screen_lg.jpg' alt='UPME' /> </div>
        <div class='workDescription col-md-4 col-sm-12 col-xs-12'>
          <div class='workDiscriptionTitle'> <strong>CLIENT</strong> <br />
            UPME Social 
          </div>
          <br />
          <p>UPME Social is one of the coolest social networking application and definitely the most comprehensive app that our geeks lately worked on. It's an all new way to interact with your friends. The application boasts a huge range of features that literally redefine the concept of social networking for the good.</p>
          <h3>Here are few of the awesomest features that the app includes:</h3>
          <ol>
            <li> Popularity based ranking system</li>        
            <li> Group chat</li>       
            <li> Multi-Tier User communication system</li>        
            <li> User generated Media Timeline</li>      
            <li> GPS based user category filter</li>       
            <li> Search</li>       
            <li> User points reward system</li>      
            <li>Unique Emoticons</li>
          </ol>
          <p>Lots of new and exciting features to be added in the application soon.</p>
          <br />
        </div>
      </div>
    </div>
    <a class='proNav proPrev' href='Golfingindian.php' title='Previous'></a>
    <a class='proNav proNext' href='healtAssist.php' title='Next'></a>
    <!--Last Text Note-->
    <?php include ('footer.php');?>
    <!--Last Text Note-->
    <?php //include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
  </body>
</html>