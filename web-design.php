<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content='Geeky Works is an award winning Mobile and Web Application Development company in Pune. We specialise in providing bespoke design and development services'/>
    <meta name='keywords' content='Mobile Application, Web Application, Website Design Company Pune, Website Development Company Pune'/>
    <title>.:: Web Site Design Company Pune - Geeky Works::.</title>
    <?php include ('assetCss.php');?>
  </head>
  <body>
  <div id='wrapper'>
    <?php include ('headerPage.php');?>
  </div>
  <!--work container-->
    <div class='workHeaderContainer'>
      <div class='container'>
        <h1 class='workHeader'>
        WEBSITE DESIGN
        </h1>
      </div>
    </div>
    <div class='aboutServiceBox'>
      <div class='container'>
        <div class='serviceBox col-md-8 col-sm-12 col-xs-12'>
          <p>Our design’s so gooood, you may just lick the screen.
          For our geeks, screen is the canvas, mouse is the brush and the passion for creating an awe-inspiring website falls nowhere short of Picasso’s love for art. Geeky Works is an award-winning website design company that works as an extension to your business, empowering your web presence. 
          From designing a top notch website to revamping your old school web property, our team of highly skilled website designers can do it all for you. They work closely with you to thoroughly understand your mindset and deliver pages that make the word ‘Wow’ look like an understatement. <br/><br/>
          Our design process starts only once we have gained enough knowledge about your business and are in sync with your vision regarding the design. </p>
          <br />
          <p><b>Geeky Works: Bringing your brand to the forefront</b><br/>
          A single mouse click and Google lists down thousands of search results. One more click and a person can delve into the details. Two-tenths of a second is all that it takes this person, your prospective client to form a first opinion about your brand.
          </p>
          <br />
          <p> <b>Problem:</b> Too many options and very less time to impress your prospect. </p>
          <p><b>Solution:</b> Geeky Works.</p><br/>
          <p><b>Globally Yours: Serving Across Boundaries</b><br/>From New York to New Delhi, our client base is spread across continents. During the last few years our clients have really appreciated and loved our work. This gives us the confidence and valour to request you to give us a chance to serve you. <br/>
          Contact us now for any Website Design related query and let us beautify your online presence.</p>
          <br /><br />
          <a href='index.php#contact'><img src='images/conatct_button.jpg' width='200' height='80' /></a>
        </div>
        <div class='serviceImg col-md-4 col-sm-12 col-xs-12'>
          <img src='images/web_design_img.png'/>
        </div>
        <div style='clear:both;'></div>
      </div>
    </div>
    <?php include ('footer.php');?>
    <!-- /Last Text Note -->
    <?php //include ('assetPageJs.php');?>
    <?php include ('assetJs.php');?>
    </div>
  </body>
</html>